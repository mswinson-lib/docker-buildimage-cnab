FROM busybox as build-env
MAINTAINER Mark Swinson <mark@mswinson.com>

ARG dockerappversion=0.8.0
ARG dockerappbin=docker-app-standalone-linux

RUN wget https://github.com/docker/app/releases/download/v${dockerappversion}/docker-app-linux.tar.gz
RUN tar xzf docker-app-linux.tar.gz
RUN rm docker-app-linux.tar.gz
RUN mv ${dockerappbin} docker-app
RUN chown root:root docker-app
RUN ls -l

FROM docker/compose:1.22.0

# docker-app
COPY --from=build-env docker-app .
RUN cp docker-app /usr/local/bin/docker-app
RUN rm docker-app
RUN apk add make

ENTRYPOINT ["/bin/sh", "-c"]

