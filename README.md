# buildimage-cnab

    docker image for building cnab bundles

## Usage

    make build   # build image
    make deploy  # push image as snapshot to dockerhub
    make release # push image as latest version to dockerhub
         
          
