.PHONY: build clean deploy release

IMAGE_REPO=mswinson
IMAGE_NAME=buildimage-cnab
VERSION ?=`cat VERSION`
TAG=$(VERSION)
TAG_SUFFIX ?= '-develop'

BUILD_OPTIONS ?= --no-cache

DOCKERAPPVERSION=0.8.0
DOCKERAPPBIN=docker-app-standalone-linux

build:
	docker build -t $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)$(TAG_SUFFIX) --build-arg dockerappversion=$(DOCKERAPPVERSION) --build-arg dockerappbin=$(DOCKERAPPBIN) $(BUILD_OPTIONS) .

clean:
	docker rmi $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)$(TAG_SUFFIX)

deploy:
	docker login --username $(DOCKER_USER) --password $(DOCKER_PASS)
	docker push $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)$(TAG_SUFFIX)

release:
	docker tag $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)$(TAG_SUFFIX) $(IMAGE_REPO)/$(IMAGE_NAME):latest
	docker tag $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)$(TAG_SUFFIX) $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)
	docker login --username $(DOCKER_USER) --password $(DOCKER_PASS)
	docker push $(IMAGE_REPO)/$(IMAGE_NAME):latest
	docker push $(IMAGE_REPO)/$(IMAGE_NAME):$(TAG)
